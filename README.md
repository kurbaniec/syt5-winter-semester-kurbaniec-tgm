# "*syt5-winter-semester-kurbaniec-tgm*"

### Middleware Engineering

* [GK931 Middleware Engineering "Authorization and Authentication System" (MICT/BORM)](https://bitbucket.org/kurbaniec/dezsys-gk931-security-sso-kurbaniec-tgm/src/master/)

### Distributed Computing

* [GK932 Distributed Computing "High Availability System" (MICT)](https://bitbucket.org/kurbaniec/dezsys-gk932-high-availability-kurbaniec-tgm/src/master/)]
* [GK933 Distributed Computing "Cloud-Datenmanagement" (BORM)](https://bitbucket.org/kurbaniec/syt5-gk933-cloud-datamanagement-kurbaniec-tgm/src/master/)
* [GEK936 Distributed Computing "Datamanagement - Synchronisation" - EASYLIST](https://bitbucket.org/kurbaniec/syt5-gek936-datamanagement-easylist)

### Infrastructure Management

* [GK922 Fernwartung (UMAA/SABM)](https://bitbucket.org/kurbaniec/syt5-gk922-fernwartung/src/master/)

### System Management

* [GEK 92T Systemintegration "Storage Virtualization"](https://bitbucket.org/kurbaniec/syt5-gk923-storage-virtualization/src/master/)
* [GK922 Systemmanagement "Container mit Kubernetes (light)" (UMAA/SABM)](https://bitbucket.org/kurbaniec/syt5-gk914-iot-in-the-cloud-mikemeter/src/master/)
* [GK923 Systemmanagement "Shared Storage" (SABM)](https://bitbucket.org/kurbaniec/syt5-gk923-shared-storage/src/master/)

### Robotics

* [GK911 Industrielle Programmierung und Visualisierung (LISE0)](https://bitbucket.org/kurbaniec/syt5-gk911-tankentleerung/src/master/)
* [GK912 Funktionsbausteine und dynamische Visualisierung (LISE0)](https://bitbucket.org/kurbaniec/syt5-gk912-mischanlage/src/master/)

### Embedded Systems

* [GEK914 Embedded Systems "IoT in der Cloud"- MIKEMETER](https://bitbucket.org/kurbaniec/syt5-gk914-iot-in-the-cloud-mikemeter/src/master/)

### Data Science

* [Jupyter-Notebook containing all tasks](https://github.com/kurbaniec-tgm/everything-data-science)

